# Пример функционального тестирования REST сервера с использованием языка Groovy #

- https://bitbucket.org/igorNovikov/functional_testing_of_rest_server_using_groovy

	выполнил: [Игорь Новиков][Igor Novikov]

## DESCRIPTION: ##

На данном примере не рассматривается весь механизм тестирования, а так же используемых при этом технологий и инструментов. Приводится лишь пример описания кода на Groovy.

## Version ##
0.0.1

## FEATURES ##

* *Groovy*

## TODO: ##

При желании можно пойти дальше, и данный пример переписать, по принципу шаблона проектирования Компоновщик, по пока это TODO

## SYNOPSIS: ##

### Исходные данные: ###

задаются средствами используемых инструментов

```sh
context.idRegion = “5255471e2ee8568f7899d454” 
```

### Реквест «авторизация» ###

```groovy
// проверяем статус
test1 = new Meta(messageExchange)
json = test1.ifSuccess()
assert json.user != null
// получаем значение текущего пользователя и передаем значение следующему тесту
context.idUser1 = json.user.id  
```

### Реквест «список продуктов для подарка» ###

```groovy
// проверяем статус
test2 = new Meta(messageExchange)
json = test2.ifSuccess()
//Проверяем результативный ответ
assert json.products != null
assert json.products.size() > 1
assert json.productscount > 1
// проверяем основное: типы, обязательные поля и т.д.
for (iproduct in json.products) {
    test2 = new Product(iproduct)
    test2.action()
}
// проверяем значения, например наш ли регион
assert context.idRegion = json.products[0].regions[0].id
// передаем значение следующему тесту
context.idProduct == json.products[0].id 
```

### Реквест «создать желение» ###

```groovy
// проверяем статус
test3 = new Meta(messageExchange)
json = test3.ifSuccess() 
```

### Реквест «создать желание», пробуем выполнить повторно (ожидаем ошибку 404) ###

```groovy
// проверяем статус
test4 = new Meta(messageExchange)
json = test4.ifError404() 
```

### Реквест «Показать мои желания» ###

```groovy
// проверяем статус
test5 = new Meta(messageExchange)
json = test5.ifSuccess()
//Проверяем результативный ответ
assert json. wishes != null
assert json. wishes.size() > 1
assert json. wishecount > 1
// проверяем основное: типы, обязательные поля и т.д.
for (iwish in json. wishes) { 
    test5 = new Wish(iwish)
    test5.action()
}
// Получаем значение последнего желания:
context.idWish = json.wishes[0].id 
```

### Реквест «авторизация» под вторым пользователем ###

```groovy
// проверяем статус
test6 = new Meta(messageExchange)
json = test6.ifSuccess()
assert json.user != null
// получаем значение текущего пользователя
context.idUser2 = json.user.id 
```

### Реквест «Показать желания пользователя» ###

```groovy
// проверяем статус
test7 = new Meta(messageExchange)
json = test7.ifSuccess()
//Проверяем результативный ответ
assert json. wishes != null
assert json. wishes.size() > 1
assert json. wishecount > 1
// проверяем основное: типы, обязательные поля и т.д.
for (iwish in json. wishes) {
    test7 = new Wish(iwish)
    test7.action()
}
// Проверяем значение последнего желания, должно совпадать с результатом теста 5.
assert context.idWish == json.wishes[0].id
```

### Реквест «удалить своё желание», пробуем удалить чужое желание (ожидаем ошибку 403) ###

```groovy
// проверяем статус
test8 = new Meta(messageExchange)
json = test8.ifError403() 
```

Далее по аналогии, делаем подарок, на найденное желание. Проверяем подарок в списке подарков.
Создаём еще одно желание. Удаляем вновь созданное желание. Проверяем, что желание удалилось. И
т.д. пока хватит фантазии. 


### Используемые библиотеки ###

класс для заголовков

```groovy
import groovy.json.JsonSlurper

class Meta {
    private message
    private slurper
    private json

    Meta(Object curMessage) {
        message = curMessage 
        slurper = new JsonSlurper()
        json = slurper.parseText message.response.responseContent
        assert json.meta.status != null
    }
    def ifSuccess() {
        assert json.meta.status == "success"
        assert message.responseHeaders["#status#"].contains("HTTP/1.1 200 OK")
        json
    }
    def ifSuccess201() {
        assert json.meta.status == "success"
        assert message.responseHeaders["#status#"].contains("HTTP/1.1 201 Created")
        json
    }
    def ifError404() {
        assert json.meta.status == "error"
        assert message.responseHeaders["#status#"].contains("HTTP/1.1 404 Not Found")
    }
    def ifError403() {
        assert json.meta.status == "error"
        assert message.responseHeaders["#status#"].contains("HTTP/1.1 403 Access Denied")
    }
    def ifError400() {
        assert json.meta.status == "error"
        assert message.responseHeaders["#status#"].contains("HTTP/1.1 400 Bad Request")
    }

}
```

класс с регионами

```groovy
class Region {
    private iRecord
    Region(Object curRecord) {
        iRecord = curRecord
    }
    def general() {
        assert iRecord instanceof Object
        assert iRecord.id instanceof String
        assert iRecord.name instanceof String
    }
    def action() {
        general()
    }

} 
```

класс с подарками

```groovy
class Present {
    private ipresent
    private iproduct
    private ilocation
    private iactor

    Present(Object curPresent) {
        this.ipresent = curPresent 
    }
    def general() {
        assert ipresent instanceof Object
        assert ipresent.id instanceof String
        assert ipresent.code instanceof String
        if (ipresent.status != null) {
            assert ipresent.status == "received" || ipresent.status == "expired"
        }
        assert ipresent.creationtime instanceof Long || ipresent.creationtime instanceof Integer
        assert ipresent.expirationtime instanceof Long || ipresent.expirationtime instanceof Integer
        if (ipresent.receivedtime != null) {
            assert ipresent.receivedtime instanceof Long || ipresent.receivedtime instanceof Integer
        }
        if (ipresent.message != null) {
            assert ipresent.message instanceof String
        }
    }
    def product() {
        assert iproduct.id instanceof String
        assert iproduct.name instanceof String
        if (iproduct.description != null) {
            assert iproduct.description instanceof String
        }
        assert iproduct.media.url.subSequence(0,4) == 'http'
        assert iproduct.type == "product" || ipresent.product.type == "certificate"
    }
    def location() {
        assert ilocation.id instanceof String
        assert ilocation.name instanceof String
        assert ilocation.media.url.subSequence(0,4) == 'http'
        assert ilocation.address.country instanceof String
        assert ilocation.address.region instanceof String
        assert ilocation.address.street instanceof String
        assert ilocation.coords.lat instanceof Float
        assert ilocation.coords.lon instanceof Float
    }
    def actor() {
        assert iactor.id instanceof String
        assert iactor.name instanceof String
        assert iactor.lastname instanceof String
        assert iactor.media.url.subSequence(0,4) == 'http'
        assert iactor.address.country instanceof String
        assert iactor.address.region instanceof String
    }

    def action() {
        general()
        iproduct = ipresent.product
        product()
        ilocation = ipresent.location
        location()
        if (ipresent.actor != null) { 
            iactor = ipresent.actor
            actor()
        }
    }
} 
```

класс заведений

```groovy
class Location {
    private iRecord

    Location(Object curRecord) {
        this.iRecord = curRecord
    }
    private def general() {
        assert iRecord instanceof Object
        assert iRecord.id instanceof String
        assert iRecord.name instanceof String
        assert iRecord.openinghours instanceof String
        assert iRecord.address.country instanceof String
        assert iRecord.address.region instanceof String
        assert iRecord.address.street instanceof String
        assert iRecord.media.url.subSequence(0,4) == 'http'
        assert iRecord.coords.lat instanceof Float
        assert iRecord.coords.lon instanceof Float
    }
    private def search() {
        if (iRecord.promotion != null) {
            assert iRecord.promotion instanceof Object
        }
        assert iRecord.category instanceof Object
        assert iRecord.category.id instanceof String
        assert iRecord.category.name instanceof String
        if (iRecord.distance != null) {
            assert iRecord.distance instanceof Float
        }
        assert iRecord.statistics.favoritescount instanceof Integer
        assert iRecord.statistics.subscriberscount instanceof Integer
    }
    private def region() {
        assert iRecord.statistics.presentscount instanceof Integer
        assert iRecord.categories instanceof Object
        iRecord.categories.collect {
            assert it.id instanceof String
            assert it.name instanceof String
        }
    }
    private def checkinsearch() {
        if (iRecord.checkin != null) {
            assert iRecord.checkin instanceof Object
        }
    }
    def action(type) {
        general()
        switch (type) {
            case "search": 
                search()
                break
            case "region":
                region()
                break
            case "checkinsearch":
                search()
                checkinsearch()
                break
        }
    }
 } 
```


По всем вопросам можете обращаться на [igor@sitdb.ru][igoremail] и сайпу [i.novikov78] мой профиль: [Igor Novikov] по проекту: [functional_testing_of_rest_server_using_groovy]

**Благодарю, за потраченное время, Игорь Новиков!**


   [functional_testing_of_rest_server_using_groovy]: <https://bitbucket.org/igorNovikov/functional_testing_of_rest_server_using_groovy>
   [Igor Novikov]: <https://bitbucket.org/igorNovikov>
   [igoremail]: <mailto:igor@sitdb.ru>
   [i.novikov78]: <i.novikov78>
   [git-repo-url]: <https://bitbucket.org/igorNovikov/titest.git>